#Сделать REST фото менеджер.
____________________________


 

- [X] 1 загружать фотографии авторизованным пользователям.

  - [X] 1.1 при загрузке можно указывать различную метадату: геолокацию, описание, имена людей на фото.

- [X] 2 отображать список фотографий, без мета данных.

  - [X] 2.2 фильтровать фотографии по дате.

  - [X] 2.3 фильтровать фотографии по геолокации.
  
  - [X] 2.4 фильтровать фотографии по имени человека.

- [X] 3 получать фотографию по айди с метаданными.

- [X] 4 доп задача: сделать апи автодополнение по поиску возможных имен людей присутствующих на фотографиях. 
  Пример:
передаем часть имени “Алекс”
на выходе получаем
Алекс
Алексей
Александр
Александра

Вбиваем Алексан
получаем
Александр
Александра
  - [X] 4.1 настроил админ-панель(на детальной странице возможно просматривать миниатюры фотографий)
  - [X] 4.2 добавил сортировку фотографий
  - [X] 4.3 есть возможность регистрации пользователя по адресу: http://127.0.0.1:8000/api/v1/drf-register/
  - [X] 4.4 есть возможность авторизации пользователя по адресу: http://127.0.0.1:8000/api/v1/drf-auth/login/



#####Список фотографий доступен по адресу:
http://127.0.0.1:8000/api/v1/photo/

#####Список имен доступен по адресу:
http://127.0.0.1:8000/api/v1/name/

## Запуск приложения

##Склонируйте репорзиторий

## Установка
```
pip install -r requirements.txt
```

### Применить миграции
```
python manage.py migrate
```
### Создать суперпользователя
```
python manage.py createsuperuser
```
### Запуск сервера
```
python manage.py runserver
```
  

###Tech stack

 

Django (Django==4.1.5)

Python 3.9+ (3.10)

Any SQL database (PostgreSQL 12)

DRF is optional
