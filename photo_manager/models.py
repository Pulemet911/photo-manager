from django.contrib.auth.models import User
from django.db import models


class Name(models.Model):
    """Модель имени человека на фотографии"""
    name = models.CharField(max_length=20, verbose_name='имя', blank=True, db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'имя'
        verbose_name_plural = 'имена'


class Photo(models.Model):
    """Модель фотографии"""
    photo = models.ImageField(upload_to='images/', verbose_name='фото', blank=True)
    geolocation = models.CharField(verbose_name='геолокация', max_length=255, blank=True)
    description = models.TextField(verbose_name='описание', blank=True)
    names = models.ManyToManyField(Name, blank=True, verbose_name='имена')
    date_create = models.DateField(verbose_name='дата', auto_now_add=True)
    user = models.ForeignKey(User, verbose_name='пользователь', on_delete=models.CASCADE)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = "фотография"
        verbose_name_plural = "фотографии"
