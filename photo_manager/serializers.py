from django.contrib.auth.models import User
from rest_framework import serializers
from photo_manager.models import Photo, Name


class PhotoSerializer(serializers.ModelSerializer):
    """Сериалайзер фото с метаданными"""
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Photo
        fields = ['photo', 'geolocation', 'description', 'date_create', 'names', 'user']


class OnlyPhotoSerializer(serializers.ModelSerializer):
    """Сериалайзер фото без метаданных"""
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Photo
        fields = ['id', 'photo', 'user']


class NameSerializer(serializers.ModelSerializer):
    """Сериалайзер имени"""

    class Meta:
        model = Name
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    """Сериалайзер регистрации"""

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
        )
        return user

    class Meta:
        model = User
        fields = ("id", "username", "password",)
