from django.contrib import admin
from photo_manager.models import Name, Photo
from django.utils.safestring import mark_safe


@admin.register(Name)
class NameAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    fields = ('user', 'photo', 'geolocation', 'description', 'date_create', 'get_html_photo', 'names')
    readonly_fields = ('date_create', 'get_html_photo')
    save_on_top = True
    list_display = ['id', 'user', 'photo', 'geolocation', 'description', 'date_create']

    def get_html_photo(self, object):
        if object.photo:
            return mark_safe(f"<img src='{object.photo.url}' width=50>")

    get_html_photo.short_description = 'миниатюра'

