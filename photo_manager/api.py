from django.contrib.auth import get_user_model
from rest_framework import generics, filters
from rest_framework.generics import CreateAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny
from photo_manager.models import Photo, Name
from photo_manager.serializers import PhotoSerializer, NameSerializer, OnlyPhotoSerializer, UserSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView


class CreateUserView(CreateAPIView):
    """представления для регистрации нового пользователя"""
    model = get_user_model()
    permission_classes = [AllowAny]
    serializer_class = UserSerializer


class APIListPagination(PageNumberPagination):
    """Представления для получения количества записей на странице"""
    page_size = 2
    page_size_query_param = 'page_size'
    max_page_size = 3


class NameAPIView(generics.ListCreateAPIView):
    """Представления для получения списка имен на фотографиях"""
    queryset = Name.objects.all()
    serializer_class = NameSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    pagination_class = APIListPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ('^name',)

    def get_queryset(self):
        name = self.request.GET.get('^name', '')
        queryset = self.queryset.filter(name__startswith=name)
        return queryset


class NameApiSearch(APIView):
    """Представление автодополнения по поиску возможных имен людей присутствующих на фотографиях"""
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'search_names.html'

    def get(self, request):
        queryset = Name.objects.all()
        return Response({'name': queryset})


class NameAPIDetail(generics.RetrieveUpdateDestroyAPIView):
    """представление для получения детальной информации имени"""
    queryset = Name.objects.all()
    serializer_class = NameSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class OnlyPhotoAPIList(generics.ListCreateAPIView):
    """представление для получения списка фотографий без метаданных"""
    queryset = Photo.objects.all()
    serializer_class = OnlyPhotoSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    pagination_class = APIListPagination
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend]
    filterset_fields = ['geolocation', 'date_create', 'names']
    ordering_fields = ('geolocation', 'date_create')


class PhotoAPIDetail(generics.RetrieveUpdateDestroyAPIView):
    """представление для получения детальной информации фотографии"""
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
