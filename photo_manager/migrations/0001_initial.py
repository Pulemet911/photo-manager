# Generated by Django 4.1.5 on 2023-01-16 09:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Name',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, db_index=True, max_length=20, verbose_name='имя')),
            ],
            options={
                'verbose_name': 'имя',
                'verbose_name_plural': 'имена',
            },
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='images/')),
                ('location', models.CharField(blank=True, max_length=255, verbose_name='геолокация')),
                ('description', models.TextField(blank=True, verbose_name='описание')),
                ('date_create', models.DateTimeField(auto_now_add=True, verbose_name='дата')),
                ('people_names', models.ManyToManyField(blank=True, to='photo_manager.name')),
            ],
            options={
                'verbose_name': 'фотография',
                'verbose_name_plural': 'фотографии',
            },
        ),
    ]
