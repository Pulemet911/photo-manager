from photo_manager.api import NameAPIView, OnlyPhotoAPIList, PhotoAPIDetail, NameAPIDetail, NameApiSearch
from django.urls import path


urlpatterns = [
    path('photo/', OnlyPhotoAPIList.as_view()),
    path('photo/<int:pk>/', PhotoAPIDetail.as_view()),
    path('name/', NameAPIView.as_view()),
    path('name/<int:pk>/', NameAPIDetail.as_view()),
    path('name_search/', NameApiSearch.as_view())

]

